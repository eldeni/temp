<?php
/**
 * Date: 11/13/2015
 * @author Engine
 */

namespace view;

function test($div) {
    if($div == 1) {
        include_once CONTENTPATH.$div.".php";
    }
}

/**
 * Include divs if it is set in the service layer.
 */
function load_div($div) {
    // Global and local variables.
    global $_VAL;
//    $divs = $_VAL['view']['div'];

    // Load the div if it is set in the respective service.
    if(in_array($div, $_VAL['view']['div'])) {
        load_file($div, "content");
    }
}

/**
 * Include divs if it is set in the service layer.
 */
function load_div_always($div) {
    // Global and local variables.
    global $_VAL;

    // Load the div in any condition.
    load_file($div, "content");
}


/**
 * Hide the div at the initial page load.
 */
function cls_hidden($div) {
    // Global and local variables.
    global $_VAL;
    $divs = $_VAL['view']['div'];

    // Load the div if it is set in the respective service.
    if(in_array($div, $divs)) {
        echo "";
    }
    else {
        echo "hidden";
    }

}


/**
 * Render selected list of contents into display.
 */
function contains($array) {
    global $_VAL;

    $_VAL['view']['div'] = $array;
}