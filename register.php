<div id="register-wrapper" class="">
    <div id="register" class="border-gray">
        <h4 class="border-black">register</h4>

        <form class="border-green">
            <input type="text" id="user_id" placeholder="choose a id" class="border-blue">
            <input type="text" id="user_name" placeholder="choose a name" class="border-blue"> <input type="password" id="password" placeholder="password">
            <input type="password" id="password2" placeholder="confirm password"> <input type="text" id="email" placeholder="email (optional)">
        </form>
        <button id="register-btn">register</button>
    </div>
</div>

<script>
    $(function () {

        $("#register-btn").click(function (e) {
            if ($("#save_user_id").is(":checked")) {
                Cookies.set('user_id', $("#user_id").val());
                Cookies.set('password', $("#password").val());
            } else {
                Cookies.set('user_id', '');
                Cookies.set('password', '');
            }

            var password1 = $("#password").val(),
                password2 = $("#password2").val();

            if (password1 != password2) {
                showErr('확인 암호가 일치하지 않습니다', '');
                return false;
            } else if (!chk_password(password1)) {
                showErr('암호는 숫자, 문자, 특수문자 포함 6~10자 입니다', '');
                return false;
            }

            showLoading();
            var arg1 = {
                cmd: 'register',
                user_id: $("#user_id").val(),
                user_name: $("#user_name").val(),
                pw: $("#password").val(),
                email: $("#email").val()
            };
            $.post("/process/account", arg1, function (json) {
                    hideLoading();
                    if (json.desc == "ok") {
                        location.href = '/';
                    } else {
                        showErr(json.desc, '');
                    }
                }, "json")
                .fail(function (data) {
                    hideLoading();
                    var json = JSON.parse(data.responseText);
                    showErr(json.desc);
                });
        });
    });
</script>